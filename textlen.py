import tempfile
import subprocess
from stl import mesh
import sys

# get command line arguments
font = ""
if len(sys.argv) > 1:
    font = sys.argv[1]


def measuretext(t):
    t = t.replace("\\", "\\\\").replace('"', '\\"')
    with tempfile.NamedTemporaryFile(mode="w", delete=False) as f:
        # write character to file
        f.write(f'linear_extrude(1) text("{t}", 1, "{font}");\n')
    o = tempfile.NamedTemporaryFile(delete=False)
    o.close()
    # generate stl with openscad
    subprocess.run(
        f"openscad --export-format binstl -o {o.name} {f.name}",
        shell=True,
        check=True,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
    )

    # read stl file
    m = mesh.Mesh.from_file(f"{o.name}")
    # find max and min
    mx = 0
    mn = m.points[0][0]
    for point in m.points:
        ps = point[0:3], point[3:6], point[6:9]
        for p in ps:
            if p[0] > mx:
                mx = p[0]
            if p[0] < mn:
                mn = p[0]
    return mx, mn, t


# iterate through all printable ascii characters
rslt = {}
dot_max = 0
for i in range(33, 127):
    c = chr(i)
    mx, mn, escaped = measuretext(c)
    if c == ".":
        dot_max = mx
    rslt[escaped] = round(mx + mn, 4)

# find length of space
rslt[" "] = round(measuretext(" .")[0] - dot_max, 4)


print("function charlen(c)=", end="")
vs = []
for k, v in rslt.items():
    print(f'c=="{k}"?' + str(v) + ":", end="")
    vs.append(v)
# average for unknown characters
print(f"{round(sum(vs)/len(vs), 4)};")
print("function sum(v) = [for(p=v) 1]*v;")
print("function textlen(txt, size=10) = sum([for (i = txt) charlen(i)*size]);")
